require 'practica7'

describe Lista do
 
  describe " # NODO " do
 
    it 'Existe nodo' do

      Libro1 = Bibliografia::Bibliografia.new()
      L1 = Lista::Lista.new()
      L1.insert(Libro1) 
      expect(L1.nodo_ini).to_not be_nil
         
    end
  
  end

  describe " # LISTA" do
  
    it 'Se puede insertar un elemento' do
    
      Libro2 = Bibliografia::Bibliografia.new()
        L2 = Lista::Lista.new()
        L2.insert(Libro2)   
        expect(L2.nodo_ini).to_not be_nil
    
    end
    
    it 'Se pueden insertar varios elementos' do
      Libro3 = Bibliografia::Bibliografia.new()
      L3 = Lista::Lista.new()
      L3.insert(Libro2) 
      L3.insert(Libro3)
      expect(L3.nodo_ini).to_not be_nil
      expect(L3.nodo_act).to_not be_nil

    end
  
    it 'Existe cabeza' do
      Libro4 = Bibliografia::Bibliografia.new()
      L4 = Lista::Lista.new()
      L4.insert(Libro4)   
      expect(L4.cabeza).to eq(L4.nodo_act)
    end
  
    it 'Se extrae el primer elmento de la lista' do 
   
      Libro5 = Bibliografia::Bibliografia.new()
      Libro6 = Bibliografia::Bibliografia.new()

         L5 = Lista::Lista.new()
         L5.insert(Libro5)
        L5.insert(Libro6)

        nodo_auxiliar = L5.nodo_ini
        L5.extraer
        expect(L5.nodo_ini).to eq(nodo_auxiliar[1])
     
   end
   
 end

describe "# EJEMPLOS" do
    it 'Se inserta el primer objeto en la lista' do
      LIBRO01 = Bibliografia::Bibliografia.new()
      L6 = Lista::Lista.new()
      aut=%w{'Dave Thomas' 'Andy Hunt' 'Chad Fowler'}
      Tit = "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide"
      Ser = "(The Facets of Ruby)"
      Edt = "Pragmatic Bookshelf"
      Edc = "4 edition"
      fch = "(July 7, 2013)"
      num = %w{ ISBN-13:978-1937785499 ISBN-10:1937785491 }
      
      LIBRO01.setA(aut)
      LIBRO01.setT(Tit)
      LIBRO01.setS(Ser)
      LIBRO01.setEdt(Edt)
      LIBRO01.setEdc(Edc)
      LIBRO01.setFecha(fch)
      LIBRO01.setNum(num)
     
       L6.insert(LIBRO01)
       expect(L6.cabeza).to_not be_nil
       expect(L6.cabeza).to eq(L6.nodo_act)
   end
       
    it 'Se inserta el segundo objeto en la lista' do
      LIBRO02 = Bibliografia::Bibliografia.new()
      aut1=%w{ "Scott Chacon" }
      Tit1 = "Pro Git 2009th Edition"
      Ser1 = "(Pro)"
      Edt1 = "Apress"
      Edc1 = "2009 edition"
      fch1 = "(August 27, 2009)"
      num1 = %w{ ISBN-13:978-1430218333 ISBN-10:1430218339 }
    
      LIBRO02.setA(aut1)
      LIBRO02.setT(Tit1)
      LIBRO02.setS(Ser1)
      LIBRO02.setEdt(Edt1)
      LIBRO02.setEdc(Edc1)
      LIBRO02.setFecha(fch1)
      LIBRO02.setNum(num1)
     
       L6.insert(LIBRO02)
       expect(L6.cabeza).to_not be_nil
       expect(L6.cabeza).to eq(L6.nodo_act)
       expect(L6.cabeza).to_not eq(L6.nodo_ini)
       
    end  
      
       
    it 'Se inserta el tercer objeto en la lista' do 
      LIBRO03 = Bibliografia::Bibliografia.new()
      aut2=%w{ "David Flanagan" "Yukihiro Matsumoto" }
      Tit2 = "The Ruby Programming Language"
      Edt2 = "O’Reilly Media"
      Edc2 = "1 edition"
      fch2 = "(February 4, 2008)"
      num2 = %w{ ISBN-10:0596516177 ISBN-13:978-0596516178 }
   
      LIBRO03.setA(aut2)
      LIBRO03.setT(Tit2)
     
      LIBRO03.setEdt(Edt2)
      LIBRO03.setEdc(Edc2)
      LIBRO03.setFecha(fch2)
      LIBRO03.setNum(num2)
     
       L6.insert(LIBRO03)
       expect(L6.cabeza).to_not be_nil
       expect(L6.cabeza).to eq(L6.nodo_act)
       expect(L6.cabeza).to_not eq(L6.nodo_ini)

    
    end
      
    
    it 'Se inserta el cuarto objeto en la lista' do 
      LIBRO04 = Bibliografia::Bibliografia.new()
      aut3=%w{ "David Chelimsky" "Dave Astels" "Bryan Helmkamp" "Dan North" "Zach Dennis" "Aslak Hellesoy" }
      Tit3 = "The RSpecBook: Behaviour Driven Development with RSpec, Cucumber, and Friends (The Facets of Ruby)"
      Edt3 = "Pragmatic Bookshelf"
      Edc3 = "1 edition"
      fch3 = "(December 25, 2010)"
      num3 = %w{ ISBN-10:1934356379 ISBN-13:978-1934356371 }
  
      LIBRO03.setA(aut3)
      LIBRO03.setT(Tit3)
     
      LIBRO03.setEdt(Edt3)
      LIBRO03.setEdc(Edc3)
      LIBRO03.setFecha(fch3)
      LIBRO03.setNum(num3)
     
       L6.insert(LIBRO04)
       expect(L6.cabeza).to_not be_nil
       expect(L6.cabeza).to eq(L6.nodo_act)
       expect(L6.cabeza).to_not eq(L6.nodo_ini)
    
    end  
    
    
     it 'Se inserta el quinto objeto en la lista' do 
      LIBRO05 = Bibliografia::Bibliografia.new()
      aut4=%w{ "Richard E" }
      Tit4 = "Silverman Git Pocket Guide"
      Edt4 = "O’Reilly Media"
      Edc4 = "1 edition"
      fch4 = "(August 2, 2013)"
      num4 = %w{ ISBN-10:1449325866 ISBN-13:978-1449325862 }

      LIBRO05.setA(aut4)
      LIBRO05.setT(Tit4)
      
      LIBRO05.setEdt(Edt4)
      LIBRO05.setEdc(Edc4)
      LIBRO05.setFecha(fch4)
      LIBRO05.setNum(num4)
     
       L6.insert(LIBRO05)
       expect(L6.cabeza).to_not be_nil
       expect(L6.cabeza).to eq(L6.nodo_act)
       expect(L6.cabeza).to_not eq(L6.nodo_ini)
    
    end  
  end
end